## Instructions to run the application

## Installing
First, install the dependencies running:

```bash
npm install
# or
yarn
```

## Run the server
Once you have installed all of the dependencies run the server:

```bash
npm run dev
# or
yarn dev
```

## Open the browser
Open [http://localhost:3000](http://localhost:3000) with your browser to see the application running.