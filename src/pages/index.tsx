import { FormEvent, useEffect, useState } from 'react'
import { api } from '../services/api'
import styles from '../styles/Home.module.scss'
import { StudentItem } from '../components/StudentItem'
import { SearchBar } from '../components/SearchBar'

interface Student {
  id: string;
  pic: string;
  firstName: string;
  lastName: string;
  email: string;
  skill: string;
  company: string;
  grades: string[];
  tags?: string[];
}

interface Tag {
  id: number;
  studentId: string;
  title: string;
}

interface TagTitle {
  index: number;
  title: string;
}

export default function Home (){
  const [students, setStudents] = useState<Student[]>([])
  const [filteredName, setFilteredName] = useState("");
  const [filteredTag, setFilteredTag] = useState("");
  const [showDetails, setShowDetails] = useState(false);
  const [selectedStudentDetail, setSelectedStudentDetail] = useState("");
  const [tags, setTags] = useState<Tag[]>([]);
  const [tagTitle, setTagTitle] = useState({} as TagTitle);
  const [isLoading, setIsLoading] = useState(true);

  function getAverageGrade(grades: string[]){
    const sum:any = grades.reduce((a, b):any => { return parseInt(a) + parseInt(b) })
    const average = sum / grades.length
    return average
  }

  async function getStudents(){
    setIsLoading(true)
    try {
      const { data } = await api.get('https://api.hatchways.io/assessment/students');
      if (data) {
        setStudents(data.students)
        setIsLoading(false)
      }
    } catch (error) {
      console.log(error)
      setIsLoading(false)
    }
  }

  function addTag(event: FormEvent, id: string) {
    event.preventDefault()

    const studetdsUpdated = students.map(student => {
      if (student.id === id) {
        if (student.tags) {
          student.tags.push(tagTitle.title)
        } else {
          return {
            ...student,
            tags: [tagTitle.title]
          }
        }
      }

      return student
    })
    
    const newTag = [
      ...tags,
      {
        id: Math.floor(Math.random() * 1000),
        studentId: id,
        title: tagTitle.title
      }
    ]
    
    setTagTitle({} as TagTitle)
    setTags(newTag)

    setStudents(studetdsUpdated)
  }

  function handleShowDetails(id: string){
    setSelectedStudentDetail(id);
    setShowDetails(!showDetails)
  }

  function handleChange(e: FormEvent, index: number) {
    let enterEvt = e.target as HTMLButtonElement

    const tagTitle = {
      index: index,
      title: enterEvt.value
    }
    setTagTitle(tagTitle)
  }

  useEffect(() => {
    getStudents()
  },[])

  return (
    <div className={styles.container}>
      <div className={styles.studentsList}>
        {isLoading &&
          <img className={styles.loadingSpinner} src="loading-spinner.gif" alt="Loading..." />
        }
        <SearchBar 
          handleChange={setFilteredName}
          placeholder="Search by name"
        />

        <SearchBar 
          handleChange={setFilteredTag}
          placeholder="Search by tag"
        />
        
        {students.filter(student => {

          if (filteredName === "") {
            if (filteredTag) {
              if (student.tags?.filter(tag => tag.includes(filteredTag.toLocaleLowerCase()))) {
                return student
              }
            } else {
              return student
            }
          } else if (filteredTag === "") {
            if (filteredName) {
              if (student.firstName.toLowerCase().includes(filteredName.toLowerCase()) ||
                student.lastName.toLowerCase().includes(filteredName.toLowerCase())) {
                return student
              }
            } else {
              return student
            }
          } else if (filteredName && filteredTag) {
            if (student.tags) {
              if (student.firstName.toLowerCase().includes(filteredName.toLowerCase()) ||
                  student.lastName.toLowerCase().includes(filteredName.toLowerCase()) &&
                  student.tags?.filter(tag => tag.includes(filteredTag.toLowerCase()))) {
               return student
              }
            }
          }

        }).map((student, index) => (
          <StudentItem 
            key={student.id}
            student={student}
            getAverageGrade={getAverageGrade} 
            showDetails={showDetails} 
            selectedStudentDetail={selectedStudentDetail}
            addTag={addTag}
            handleChange={handleChange} 
            index={index}
            tagTitle={tagTitle}
            handleShowDetails={handleShowDetails}
            tags={tags}
          />
        ))}
      </div>
    </div>
  )
}