import styles from '../styles/Home.module.scss'

interface SearchBarProps {
    handleChange: (value: string) => void;
    placeholder: string;
}

export function SearchBar({ handleChange, placeholder }: SearchBarProps){
    return (
        <div className={styles.searchInputContainer}>
          <input 
            className={styles.searhInput} 
            type="text" 
            placeholder={placeholder}
            onChange={(e) => handleChange(e.target.value)} 
          />
        </div>
    )
}