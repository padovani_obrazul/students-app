import { FormEvent } from 'react';
import styles from '../styles/Home.module.scss'

interface StudentItemProps {
    student: {
        id: string;
        pic: string;
        firstName: string;
        lastName: string;
        email: string;
        skill: string;
        company: string;
        grades: string[];
        tags?: string[];
    },
    getAverageGrade: (grades: string[]) => number;
    showDetails: boolean;
    selectedStudentDetail: string;
    addTag: (e: FormEvent, id: string) => void;
    handleChange: (e: FormEvent, index: number) => void;
    index: number;
    tagTitle: {
        index: number;
        title: string;
    },
    handleShowDetails: (id: string) => void;
    tags: Tag[];
}

interface Tag {
    id: number;
    studentId: string;
    title: string;
}

export function StudentItem({ 
  getAverageGrade,
  addTag,
  handleChange,
  handleShowDetails,
  student, 
  showDetails,
  selectedStudentDetail,
  index,
  tagTitle,
  tags
}: StudentItemProps){
  return (
    <div key={student.id} className={`${styles.studentItem} ${showDetails && selectedStudentDetail === student.id && styles.showDetails}`}>
      <div className={styles.sideImage}>
        <img src={student.pic} alt="student picture" />
      </div>
      <div className={styles.studentContent}>
        <strong>{student.firstName} {student.lastName}</strong>
        <div className={styles.studentInfo}>
          <div>Email: {student.email}</div>
          <div>Company: {student.company}</div>
          <div>Skill: {student.skill}</div>
          <div>Average: {getAverageGrade(student.grades)}%</div>
          <div className={styles.tagsAdded}>{tags.filter(tag => {
            if (tag.studentId === student.id) {
              return tag.title
            }
          }).map(tag => (
            <div className={styles.tag} key={tag.id}>
              <span>{tag.title}</span>
            </div>
          ))}</div>
          <form onSubmit={(event) => addTag(event, student.id)}>
            <input
              className={styles.addTagInput}
              type="text" 
              placeholder="add tag"
              onChange={(e) => handleChange(e, index)}
              value={tagTitle.index === index ? tagTitle.title : ""}
            />
            <button className={styles.submitTag} type="submit"></button>
          </form>

          {showDetails && selectedStudentDetail === student.id ? 
            <div className={styles.grades}>
              {student.grades.map((grade, key) =>(
                <div key={key} className={styles.gradesContainer}>
                  <div>Test {key + 1}: </div>
                  <div>{grade}%</div>
                </div>
              ))}
            </div>
          : null}
        </div>
      </div>
      <div className={styles.detailsBtn}>
        <button 
          type="button" 
          className={selectedStudentDetail === student.id && showDetails ? `${styles.expansionBtn} ${styles.expanded}` : styles.expansionBtn}
          onClick={() => handleShowDetails(student.id)}
        ></button>
      </div>
    </div>
  )
}